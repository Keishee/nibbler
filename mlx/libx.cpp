/* 
 * File:   libx.cpp
 * Author: calvez_k
 * 
 * Created on March 26, 2015, 12:20 PM
 */

#include "libx.hpp"

pthread_mutex_t g_m3 = PTHREAD_MUTEX_INITIALIZER;

int gere_expose(void *mlx) {
    void *mlx_ptr = static_cast<libx *> (mlx)->getMlx_ptr();
    void *win_ptr = static_cast<libx *> (mlx)->getWin_ptr();
    void *img_ptr = static_cast<libx *> (mlx)->getImg_ptr();

    mlx_put_image_to_window(mlx_ptr, win_ptr, img_ptr, 0, 0);
}

int gere_expose_key(void *mlx) {
    pthread_mutex_lock(&g_m3);
    void *mlx_ptr = static_cast<libx *> (mlx)->getMlx_ptr();
    void *win_ptr = static_cast<libx *> (mlx)->getWin_ptr();
    void *img_ptr = static_cast<libx *> (mlx)->getImg_ptr();

    usleep(1000);
    mlx_put_image_to_window(mlx_ptr, win_ptr, img_ptr, 0, 0);
    pthread_mutex_unlock(&g_m3);
}

int gere_key(int key, void *mlx) {

    void *win_ptr = static_cast<libx *> (mlx)->getWin_ptr();
    if (key == EXIT)
        exit(1);
    else if (key == MLEFT)
        Board::getInstance(2).getSnake()->setNextDirection(LEFT);
    else if (key == MRIGHT)
        Board::getInstance(2).getSnake()->setNextDirection(RIGHT);
    if (g_arrows) {
        if (key == MUP) {
            Board::getInstance(2).getSnake()->setNextDirection(UP);
        } else if (key == MDOWN) {
            Board::getInstance(2).getSnake()->setNextDirection(DOWN);
        }
    }
    pthread_mutex_unlock(&g_m1);
    return (0);
}

void *eventhandler(void *mlx) {
    void *mlx_ptr = static_cast<libx *> (mlx)->getMlx_ptr();
    void *win_ptr = static_cast<libx *> (mlx)->getWin_ptr();
    mlx_key_hook(win_ptr, reinterpret_cast<int(*)()> (gere_key), mlx);
    mlx_loop_hook(mlx_ptr, reinterpret_cast<int(*)()> (gere_expose_key), mlx);
    mlx_loop(mlx_ptr);
}

void *libx::getWin_ptr() {
    return (this->win_ptr);
}

void *libx::getMlx_ptr() {
    return (this->mlx_ptr);
}

void *libx::getImg_ptr() {
    return (this->img_ptr);
}

libx::libx() {

    int x = 0;
    int y = 0;
    
    if ((this->mlx_ptr = mlx_init()) == 0)
        throw GraphicError("Couldn't init MLX");
    if ((this->win_ptr = mlx_new_window(this->mlx_ptr, WINDOW_SIZE, WINDOW_SIZE, (char *) "Nibbler")) == 0)
        throw GraphicError("Couldn't new window");
    if ((this->img_ptr = mlx_new_image(this->mlx_ptr, WINDOW_SIZE, WINDOW_SIZE)) == 0)
        throw GraphicError("Couldn't new image");
    this->data = mlx_get_data_addr(this->img_ptr, &this->bpp, &this->sizeline, &this->endian);
    mlx_expose_hook(this->win_ptr, reinterpret_cast<int(*)()> (gere_expose), this);
    init_window();
    pthread_create(&thread, NULL, eventhandler, this);
}

libx::~libx() {
}

void libx::init_window() {
    int x = 0;
    int y = 0;
    unsigned long i = 0;

    usleep(10000);
    while (x <= WINDOW_SIZE) {
        y = 0;
        while (y <= WINDOW_SIZE) {
            i = (y * this->sizeline) + (x * 4);
            this->data[i++] = 55;
            this->data[i++] = 0;
            this->data[i++] = 40;
            this->data[i] = 0;
            ++y;
        }
        ++x;
    }
    mlx_put_image_to_window(this->mlx_ptr, this->win_ptr, this->img_ptr, 0, 0);
}

unsigned int libx::draw_body(int x, int y, unsigned int i) {
    i = (y * this->sizeline) + (x * 4);
    this->data[i++] = 127;
    this->data[i++] = 247;
    this->data[i++] = 127;
    this->data[i] = 0;
    return (i);
}

unsigned int libx::draw_head(int x, int y, unsigned int i) {
    i = (y * this->sizeline) + (x * 4);
    this->data[i++] = 36;
    this->data[i++] = 36;
    this->data[i++] = 164;
    this->data[i] = 0;
    return (i);
}

unsigned int libx::draw_obstacle(int x, int y, unsigned int i) {
    i = (y * this->sizeline) + (x * 4);
    this->data[i++] = rand() * y % 255;
    this->data[i++] = rand() * x % 255;
    this->data[i++] = rand() * (y * x) % 255;
    this->data[i] = 0;
    return (i);
}

unsigned int libx::draw_food(int x, int y, unsigned int i) {
    i = (y * this->sizeline) + (x * 4);
    this->data[i++] = 13;
    this->data[i++] = 205;
    this->data[i++] = 205;
    this->data[i] = 0;
    return (i);
}

extern "C" {

    void libx::updateDisplay(const Snake* snake, const std::vector<Body*>& eat, const std::vector<Body*>& obstacle) {
        pthread_mutex_lock(&g_m3);

        init_window();
        int x = 0;
        int y = 0;
        unsigned int a = 0;

        for (unsigned int i = 0; i < snake->getSnake().size(); ++i) {
            x = snake->getSnake().at(i)->getPosX() * (g_entitySizex);
            y = snake->getSnake().at(i)->getPosY() * (g_entitySizey);
            while (x < ((snake->getSnake().at(i)->getPosX() * g_entitySizex) + g_entitySizex)) {
                y = snake->getSnake().at(i)->getPosY() * (g_entitySizey);
                while (y < ((snake->getSnake().at(i)->getPosY() * g_entitySizey) + g_entitySizey)) {
                    if (snake->getSnake().at(i)->getType() == HEAD) {
                        a = draw_head(x, y, a);
                    } else
                        a = draw_body(x, y, a);
                    ++y;
                }
                ++x;
            }
        }
        a = 0;
        for (unsigned int i = 0; i < eat.size(); ++i) {
            x = eat.at(i)->getPosX() * (g_entitySizex);
            y = eat.at(i)->getPosY() * (g_entitySizey);
            while (x < ((eat.at(i)->getPosX() * g_entitySizex) + g_entitySizex)) {
                y = eat.at(i)->getPosY() * (g_entitySizey);
                while (y < ((eat.at(i)->getPosY() * g_entitySizey) + g_entitySizey)) {
                    a = draw_food(x, y, a);
                    ++y;
                }
                ++x;
            }

        }
        a = 0;
        for (unsigned int i = 0; i < obstacle.size(); ++i) {
            x = obstacle.at(i)->getPosX() * (g_entitySizex);
            while (x < ((obstacle.at(i)->getPosX() * g_entitySizex) + g_entitySizex)) {
                y = obstacle.at(i)->getPosY() * (g_entitySizey);
                while (y < ((obstacle.at(i)->getPosY() * g_entitySizey) + g_entitySizey)) {
                    a = draw_obstacle(x, y, a);
                    ++y;
                }
                ++x;
            }
        }
        gere_expose(this);
        pthread_mutex_unlock(&g_m3);
    }
}

IDisplayModule::~IDisplayModule() {
    ;
}

std::string const & libx::getName() const {
    ;
}

extern "C" {

    IDisplayModule *createDisplayModule() {
        libx *mlx = new libx;
        return (mlx);
    }
}

void libx::playSoundObstacle() {

}

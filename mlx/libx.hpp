/* 
 * File:   libx.h
 * Author: calvez_k
 *
 * Created on March 26, 2015, 12:20 PM
 */

#ifndef LIBX_HPP
#define	LIBX_HPP

#include <iostream>
#include "../src/Snake.hpp"
#include "../src/Body.hpp"
#include "../src/Errors.hpp"
#include "../src/Board.hpp"
#include "mlx.h"
#include "mlx_int.h"
#include "../src/dlibs/IDisplayModule.hpp"

#define EXIT 65307
#define MLEFT 65361
#define MUP 65362
#define MRIGHT 65363
#define MDOWN 65364

class libx : public IDisplayModule {
public:
    libx();
    virtual ~libx();
    void init_window();
    void *getWin_ptr();
    void *getMlx_ptr();
    void *getImg_ptr();
    unsigned int draw_body(int x, int y, unsigned int i);
    unsigned int draw_head(int x, int y, unsigned int i);
    unsigned int draw_food(int x, int y, unsigned int i);
    unsigned int draw_obstacle(int x, int y, unsigned int i);
    virtual void updateDisplay(const Snake*, const std::vector<Body*>&, const std::vector<Body*>&);
    virtual std::string const & getName() const;
    std::vector<Body *> eat;
    std::vector<Body *> obs;
    const Snake *sn;

    virtual void playSoundObstacle();

private:
    void *mlx_ptr;
    void *win_ptr;
    void *img_ptr;
    int sizeline;
    char *data;
    void *img;
    int bpp;
    int endian;
    pthread_t thread;
};


int gere_key(int, void *);
int gere_expose(void *);

#endif	/* LIBX_HPP */


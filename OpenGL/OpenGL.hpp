/* 
 * File:   Sdl.hpp
 * Author: barbis_j
 *
 * Created on March 25, 2015, 5:35 PM
 */

#ifndef SDL_HPP
#define	SDL_HPP

#include <SDL/SDL.h>
#include <iostream>
#include "../src/Errors.hpp"
#include "../src/Board.hpp"
#include "../src/dlibs/IDisplayModule.hpp"

class OpenGL : public IDisplayModule {
private:
    pthread_t thread;

public:
    OpenGL();
    OpenGL(const OpenGL& orig);
    virtual ~OpenGL();
    OpenGL& operator=(const OpenGL& orig);

    void drawFloor();
    void drawSquare(int, int, eBodyType);
    virtual void updateDisplay(const Snake*, const std::vector<Body*>&, const std::vector<Body*>&);

    virtual void playSoundObstacle();

};

#endif	/* SDL_HPP */


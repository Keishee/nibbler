/* 
 * File:   Sdl.cpp
 * Author: barbis_j
 * 
 * Created on March 25, 2015, 5:35 PM
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <unistd.h>
#include <stdexcept>
#include "OpenGL.hpp"

bool g_arrows;
void *eventHandler(void *);

OpenGL::OpenGL() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        throw GraphicError("Couldn't init OpenGL");
    SDL_WM_SetCaption("Nibbler", NULL);
    SDL_SetVideoMode(800, 600, 32, SDL_OPENGL);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluPerspective(70, (double) 800 / 600, 1, 1000);
    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    gluLookAt(20 + (g_sizex * 2), //CAM X
            g_sizey / 2 - 5, //CAM Y
            20 + (g_sizex / 3), //CAM Z
            2, //LOOKAT X
            g_sizey / 2 - 5, //LOOKAT Y
            0, //LOOKAT Z
            0, //IDK
            0, //IDK
            1); //IDK

    pthread_create(&thread, NULL, eventHandler, NULL);
}

OpenGL::OpenGL(const OpenGL& orig) {
    (void) orig;
}

OpenGL::~OpenGL() {
    pthread_join(thread, NULL);
    SDL_Quit();
}

// OPERATORS ===================================================================

OpenGL& OpenGL::operator=(const OpenGL& orig) {
    if (this == &orig)
        return *this;
    return *this;
}

// FUNCTIONS ===================================================================

void OpenGL::drawFloor() {
    glPushMatrix();

    glTranslatef(g_sizey - 10, g_sizex - 10, -1);

    glBegin(GL_QUADS);
    glColor3ub(255, 255, 255);
    glVertex3i(-g_sizey, -g_sizex, -1);
    glVertex3i(g_sizey, -g_sizex, -1);
    glVertex3i(g_sizey, g_sizex, -1);
    glVertex3i(-g_sizey, g_sizex, -1);
    glEnd();

    glPopMatrix();
}

void OpenGL::drawSquare(int posx, int posy, eBodyType type) {
    int red, green, blue;

    if (type == HEAD) {
        red = 235;
        green = 30;
        blue = 30;
    } else if (type == REGULAR) {
        red = green = blue = 150;
    } else if (type == FOOD) {
        red = 30;
        green = 235;
        blue = 30;
    } else {
        red = 143;
        green = 30;
        blue = 235;
    }

    glPushMatrix();
    glTranslatef(posy * 2 - 10 + 1, posx * 2 - 10 + 1, -1);

    glBegin(GL_QUADS);

    // FACE DROITE
    glColor3ub(red, green, blue);
    glVertex3d(1, 1, 1);
    glVertex3d(1, 1, -1);
    glVertex3d(-1, 1, -1);
    glVertex3d(-1, 1, 1);

    // FACE DEVANT
    glColor3ub(red - 30, green - 30, blue - 30);
    glVertex3d(1, -1, 1);
    glVertex3d(1, -1, -1);
    glVertex3d(1, 1, -1);
    glVertex3d(1, 1, 1);

    // FACE GAUCHE
    glColor3ub(red - 20, green - 20, blue - 20);
    glVertex3d(-1, -1, 1);
    glVertex3d(-1, -1, -1);
    glVertex3d(1, -1, -1);
    glVertex3d(1, -1, 1);

    // FACE DERRIERE
    glColor3ub(red - 10, green - 10, blue - 10);
    glVertex3d(-1, 1, 1);
    glVertex3d(-1, 1, -1);
    glVertex3d(-1, -1, -1);
    glVertex3d(-1, -1, 1);

    // BOTTOM (USELESS)
    glColor3ub(red + 10, green + 10, blue + 10);
    glVertex3d(1, 1, -1);
    glVertex3d(1, -1, -1);
    glVertex3d(-1, -1, -1);
    glVertex3d(-1, 1, -1);

    // TOP
    glColor3ub(red + 20, green + 20, blue + 20);
    glVertex3d(1, -1, 1);
    glVertex3d(1, 1, 1);
    glVertex3d(-1, 1, 1);
    glVertex3d(-1, -1, 1);

    glEnd();

    glPopMatrix();
}

extern "C" {

    void OpenGL::updateDisplay(const Snake* snake, const std::vector<Body*>& _food, const std::vector<Body*>& obstacles) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        this->drawFloor();

        for (unsigned int i = 0; i < snake->getSnake().size(); ++i) {
            this->drawSquare(snake->getSnake().at(i)->getPosX(),
                    snake->getSnake().at(i)->getPosY(),
                    snake->getSnake().at(i)->getType());
        }
        for (unsigned int i = 0; i < _food.size(); ++i) {
            this->drawSquare(_food.at(i)->getPosX(),
                    _food.at(i)->getPosY(),
                    _food.at(i)->getType());
        }
        for (unsigned int i = 0; i < obstacles.size(); ++i) {
            this->drawSquare(obstacles.at(i)->getPosX(),
                    obstacles.at(i)->getPosY(),
                    obstacles.at(i)->getType());
        }

        glFlush();
        SDL_GL_SwapBuffers();
    }
}
// NON-CLASS FUNCTIONS =========================================================

void *eventHandler(void *arg) {
    SDL_Event event;

    (void) arg;
    while (1) {
        SDL_WaitEvent(&event);
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_RIGHT) {
                Board::getInstance(2).getSnake()->setNextDirection(RIGHT);
            } else if (event.key.keysym.sym == SDLK_LEFT) {
                Board::getInstance(2).getSnake()->setNextDirection(LEFT);
            } else if (event.key.keysym.sym == SDLK_ESCAPE) {
                exit(1);
            }
            if (g_arrows) {
                if (event.key.keysym.sym == SDLK_UP) {
                    Board::getInstance(2).getSnake()->setNextDirection(UP);
                } else if (event.key.keysym.sym == SDLK_DOWN) {
                    Board::getInstance(2).getSnake()->setNextDirection(DOWN);
                }
            }
            pthread_mutex_unlock(&g_m1);
        }
    }
    return NULL;
}

IDisplayModule::~IDisplayModule() {

}

void OpenGL::playSoundObstacle() {

}

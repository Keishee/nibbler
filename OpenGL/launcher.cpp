/* 
 * File:   testSDL.cpp
 * Author: barbis_j
 *
 * Created on March 25, 2015, 5:44 PM
 */

#include "OpenGL.hpp"

extern "C" {

    IDisplayModule *createDisplayModule() {
        std::cout << "openGL launcher" << std::endl;
        OpenGL *opengl = new OpenGL;
        return opengl;
    }
}
/* 
 * File:   Sdl.cpp
 * Author: barbis_j
 * 
 * Created on March 25, 2015, 5:35 PM
 */

#include "Sdl.hpp"


int g_entitySizex;
int g_entitySizey;

void *eventHandler(void *arg) {
    SDL_Event event;

    Sdl sdl = *(Sdl *) (arg);
    while (1) {
        SDL_WaitEvent(&event);
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_RIGHT) {
                Board::getInstance(2).getSnake()->setNextDirection(RIGHT);
            } else if (event.key.keysym.sym == SDLK_LEFT) {
                Board::getInstance(2).getSnake()->setNextDirection(LEFT);
            } else if (event.key.keysym.sym == SDLK_ESCAPE) {
                exit(1);
            }
            if (g_arrows) {
                if (event.key.keysym.sym == SDLK_UP) {
                    Board::getInstance(2).getSnake()->setNextDirection(UP);
                } else if (event.key.keysym.sym == SDLK_DOWN) {
                    Board::getInstance(2).getSnake()->setNextDirection(DOWN);
                }
            }
            pthread_mutex_unlock(&g_m1);
        }
    }
    return NULL;
}

Sdl::Sdl() {
    if ((SDL_Init(SDL_INIT_VIDEO)) < 0)
        throw GraphicError("Couldn't init SDL");
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) {
        throw GraphicError(Mix_GetError());
    }

    SDL_WM_SetCaption("Nibbler", NULL);
    screen = SDL_SetVideoMode(WINDOW_SIZE, WINDOW_SIZE, 8, SDL_SWSURFACE | SDL_DOUBLEBUF);
    head = SDL_LoadBMP("SDL/snakehead.bmp");
    body = SDL_LoadBMP("SDL/snakebody.bmp");
    obstacle = SDL_LoadBMP("SDL/obstacle.bmp");
    food = SDL_LoadBMP("SDL/food.bmp");
    background = SDL_LoadBMP("SDL/bg.bmp");

    head = Sdl::ScaleSurface(head, g_entitySizex, g_entitySizey);
    body = Sdl::ScaleSurface(body, g_entitySizex, g_entitySizey);
    food = Sdl::ScaleSurface(food, g_entitySizex, g_entitySizey);
    obstacle = Sdl::ScaleSurface(obstacle, g_entitySizex, g_entitySizey);

    musique = Mix_LoadMUS("SDL/mgsSound.wav");
    theme = Mix_LoadMUS("SDL/mgTheme.wav");

    SDL_SetColorKey(food, SDL_SRCCOLORKEY, 0x191919);
    SDL_SetColorKey(head, SDL_SRCCOLORKEY, 0x191919);
    SDL_SetColorKey(body, SDL_SRCCOLORKEY, 0x191919);
    SDL_SetColorKey(obstacle, SDL_SRCCOLORKEY, 0xffffff);
    std::cout << SDL_GetError() << std::endl;
    pos.x = 0;
    pos.y = 0;
    SDL_BlitSurface(background, NULL, screen, &pos);
    SDL_Flip(screen);
    if (theme)
      Mix_PlayMusic(theme, -1);
    pthread_create(&thread, NULL, eventHandler, this);
}

Sdl::Sdl(const Sdl & orig) {
    (void) orig;
}

Sdl::~Sdl() {
    Mix_FreeMusic(musique);
    Mix_CloseAudio();
    SDL_Quit();
}

Sdl & Sdl::operator=(const Sdl & orig) {
    if (this == &orig)
        return *this;
    return *this;
}

Uint32 Sdl::getpixel(SDL_Surface *surface, int x, int y) {
    int bpp = surface->format->BytesPerPixel;
    void *p = (void *) ((char *) surface->pixels + (y * surface->pitch) + (x * bpp));
    return *(unsigned int *) p;
}

void Sdl::putpixel(SDL_Surface *surface, int x, int y, unsigned int pixel) {
    int bpp = surface->format->BytesPerPixel;
    void *p = (void *) ((char *) surface->pixels + (y * surface->pitch) + (x * bpp));
    *(unsigned int *) p = pixel;
}

SDL_Surface * Sdl::ScaleSurface(SDL_Surface *Surface, unsigned int Width, unsigned int Height) {
    double stretch_size_x;
    double stretch_size_y;

    if (!Surface || !Width || !Height)
        return 0;

    SDL_Surface *_ret = SDL_CreateRGBSurface(Surface->flags, Width, Height, Surface->format->BitsPerPixel,
            Surface->format->Rmask, Surface->format->Gmask, Surface->format->Bmask, Surface->format->Amask);

    stretch_size_x = (static_cast<double> (Width) / static_cast<double> (Surface->w));
    stretch_size_y = (static_cast<double> (Height) / static_cast<double> (Surface->h));

    for (int y = 0; y < Surface->h; y++) {
        for (int x = 0; x < Surface->w; x++) {
            for (int y2 = 0; y2 < stretch_size_y; ++y2) {
                for (int x2 = 0; x2 < stretch_size_x; ++x2) {
                    Sdl::putpixel(_ret, static_cast<int> (stretch_size_x * x) + x2,
                            static_cast<int> (stretch_size_y * y) + y2, Sdl::getpixel(Surface, x, y));
                }
            }
        }
    }
    return _ret;
}

extern "C" {

    void Sdl::updateDisplay(const Snake* snake, const std::vector<Body*>& _food, const std::vector<Body*>& obstacles) {
        (void) _food;
        (void) obstacles;
        pos.x = 0;
        pos.y = 0;

        SDL_BlitSurface(background, NULL, screen, &pos);
        for (unsigned int i = 0; i < snake->getSnake().size(); ++i) {
            pos.x = snake->getSnake().at(i)->getPosX() * (g_entitySizex);
            pos.y = snake->getSnake().at(i)->getPosY() * (g_entitySizey);
            if (snake->getSnake().at(i)->getType() == HEAD) {
                SDL_BlitSurface(head, NULL, screen, &pos);
            } else
                SDL_BlitSurface(body, NULL, screen, &pos);
        }
        for (unsigned int i = 0; i < _food.size(); ++i) {
            pos.x = _food.at(i)->getPosX() * (g_entitySizex);
            pos.y = _food.at(i)->getPosY() * (g_entitySizey);
            SDL_BlitSurface(food, NULL, screen, &pos);
        }
        for (unsigned int i = 0; i < obstacles.size(); ++i) {
            pos.x = obstacles.at(i)->getPosX() * (g_entitySizex);
            pos.y = obstacles.at(i)->getPosY() * (g_entitySizey);
            SDL_BlitSurface(obstacle, NULL, screen, &pos);
        }
        SDL_Flip(screen);
    }

}

IDisplayModule::~IDisplayModule() {

}

extern "C" {

    void Sdl::playSoundObstacle() {
        Mix_PlayMusic(musique, 5);
        sleep(1);
    }
}

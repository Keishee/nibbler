/* 
 * File:   Sdl.hpp
 * Author: barbis_j
 *
 * Created on March 25, 2015, 5:35 PM
 */

#ifndef SDL_HPP
#define	SDL_HPP

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <iostream>
#include "../src/Board.hpp"
#include "../src/dlibs/IDisplayModule.hpp"
#include "../src/Timer.hpp"
#include "../src/TimeGiver.hpp"
#include "../src/Errors.hpp"

class Sdl : public IDisplayModule {
public:
    Sdl();
    Sdl(const Sdl& orig);
    virtual ~Sdl();
    Sdl& operator=(const Sdl& orig);

    Uint32 getpixel(SDL_Surface *surface, int x, int y);
    void putpixel(SDL_Surface *surface, int x, int y, unsigned int pixel);
    SDL_Surface *ScaleSurface(SDL_Surface *Surface, unsigned int Width, unsigned int Height);
    
    virtual void updateDisplay(const Snake*, const std::vector<Body*>&, const std::vector<Body*>&);
    
    void playSoundObstacle();
    
private:
    SDL_Surface *screen;
    SDL_Surface *food;
    SDL_Surface *obstacle;
    SDL_Rect pos;
    SDL_Surface *background;
    SDL_Surface *head;
    SDL_Surface *body;
    
    Mix_Music *musique;
    Mix_Music *theme;
    pthread_t thread;
};

#endif	/* SDL_HPP */


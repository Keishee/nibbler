/* 
 * File:   testSDL.cpp
 * Author: barbis_j
 *
 * Created on March 25, 2015, 5:44 PM
 */

#include "Sdl.hpp"

extern "C" {

    IDisplayModule *createDisplayModule() {
        std::cout << "sdl launcher" << std::endl;
        Sdl *sdl = new Sdl();
        return sdl;
    }
}

CC	= g++ -export-dynamic -ldl -pthread

RM	= rm -f

CPPFLAGS    += -Wall -Wextra

NAME	= nibbler

NAME2	= sdl

NAME3	= opengl

NAME4	= mlx

SRC	= src/Body.cpp	\
	  src/TimeGiver.cpp \
	  src/main.cpp	\
	  src/Snake.cpp	\
	  src/Timer.cpp	\
	  src/Errors.cpp	\
	  src/dlibs/DLLoader.cpp \
	  src/Board.cpp

OBJ	= $(SRC:.cpp=.o)



all: $(NAME)

$(NAME): $(OBJ)
	make -C mlx
	make -C OpenGL
	make -C SDL
	$(CC) -o $(NAME) $(OBJ)

$(NAME2):
	make -C SDL

$(NAME3):
	make -C OpenGL

$(NAME4):
	make -C mlx

clean:
	$(RM) $(OBJ)
	make clean -C mlx
	make clean -C OpenGL
	make clean -C SDL

fclean: clean
	$(RM) $(NAME)
	make fclean -C mlx
	make fclean -C OpenGL
	make fclean -C SDL

re: fclean all

.PHONY: all clean fclean re

/* 
 * File:   Body.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 24, 2015, 11:44 AM
 */

#include <exception>

#include "Body.hpp"
#include "nibbler.hpp"
#include "Board.hpp"

Body::Body(int x, int y, eBodyType type, eDirection direction) :
_x(x), _y(y), _direction(direction), _type(type) {

}

Body::Body(const Body& orig) :
_x(orig._x), _y(orig._y), _direction(orig._direction), _type(orig._type) {

}

Body::~Body() {
}

// OPERATORS ===================================================================

Body& Body::operator=(const Body& right) {
    if (this == &right)
        return *this;
    this->_x = right._x;
    this->_y = right._y;
    this->_direction = right._direction;
    this->_type = right._type;
    return *this;
}

// GETTERS & SETTERS ===========================================================

eDirection Body::getDirection() const {
    return this->_direction;
}

eBodyType Body::getType() const {
    return this->_type;
}

int Body::getPosX() const {
    return this->_x;
}

int Body::getPosY() const {
    return this->_y;
}

void Body::setDirection(const eDirection& direction) {
    this->_direction = direction;
}

void Body::setPos(const int& x, const int& y) {
    this->_x = x;
    this->_y = y;
}

// FUNCTIONS ===================================================================

/**
 * sets the current direction the Body is going
 * and updates its position accordingly
 * @param direction the direction
 */
int Body::setDirectionAndUpdate(const eDirection& direction) {
    this->setDirection(direction);
    switch (direction) {
        case LEFT:
            this->setPos(this->getPosX() - 1, this->getPosY());
            break;
        case RIGHT:
            this->setPos(this->getPosX() + 1, this->getPosY());
            break;
        case UP:
            this->setPos(this->getPosX(), this->getPosY() - 1);
            break;
        case DOWN:
            this->setPos(this->getPosX(), this->getPosY() + 1);
            break;
        case NONE:
            break;
        default:
            break;
    }
    if (this->getPosX() < 0 || this->getPosX() >= g_sizex ||
            this->getPosY() < 0 || this->getPosY() >= g_sizey)
        return 1;
    if (this->_type == HEAD) {
        int posx = Board::getInstance(1).getFood().at(0)->getPosX();
        int posy = Board::getInstance(1).getFood().at(0)->getPosY();
        if (this->getPosX() == posx && this->getPosY() == posy)
            return FOOD_EATED;
        posx = Board::getInstance(1).getSnake()->getSnake().at(0)->getPosX();
        posy = Board::getInstance(1).getSnake()->getSnake().at(0)->getPosY();
        for (unsigned int i = 1; i < Board::getInstance(1).getSnake()->getSnake().size(); ++i) {
            if (posx == Board::getInstance(1).getSnake()->getSnake().at(i)->getPosX() &&
                    posy == Board::getInstance(1).getSnake()->getSnake().at(i)->getPosY())
                return 1;
        }
        for (unsigned int i = 0; i < Board::getInstance(1).getObstacles().size(); ++i) {
            if (posx == Board::getInstance(1).getObstacles().at(i)->getPosX() &&
                    posy == Board::getInstance(1).getObstacles().at(i)->getPosY()) {
                Board::getInstance(1).getDisplayModule()->playSoundObstacle();
                return 1;
            }
        }
    }
    return 0;
}

/* 
 * File:   Errors.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 31, 2015, 3:47 PM
 */

#include "Errors.hpp"
// DLERROR =====================================================================

DLError::DLError(const std::string& what) throw(){
    _what = "DLError: ";
    _what += what;
}

DLError& DLError::operator=(const DLError& right) {
    this->_what = right._what;
    return *this;
}

const char* DLError::what() const throw(){
    return this->_what.c_str();
}

DLError::~DLError() throw(){
}


// GRAPHICERROR ================================================================

GraphicError::GraphicError(const std::string& what) throw(){
    _what = "GraphicError: ";
    _what += what;
}

GraphicError& GraphicError::operator=(const GraphicError& right) {
    this->_what = right._what;
    return *this;
}

const char* GraphicError::what() const throw(){
    return this->_what.c_str();
}

GraphicError::~GraphicError() throw(){
}


// STANDARDERROR ===============================================================

StandardError::StandardError(const std::string& what) throw(){
    this->_what = "StandardError: ";
    this->_what += what;
}

StandardError& StandardError::operator=(const StandardError& right) {
    this->_what = right._what;
    return *this;
}

const char* StandardError::what() const throw(){
    return this->_what.c_str();
}

StandardError::~StandardError() throw(){
}

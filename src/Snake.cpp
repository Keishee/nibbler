/* 
 * File:   Snake.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 25, 2015, 12:20 PM
 */

#include "Snake.hpp"
#include "Board.hpp"

Snake::Snake() {
    Body *head;
    Body *body;
    Body *body2;
    Body *body3;

    head = new Body(g_sizex / 2, g_sizey / 2, HEAD, RIGHT);
    body = new Body(g_sizex / 2 - 1, g_sizey / 2, REGULAR, RIGHT);
    body2 = new Body(g_sizex / 2 - 2, g_sizey / 2, REGULAR, RIGHT);
    body3 = new Body(g_sizex / 2 - 3, g_sizey / 2, REGULAR, RIGHT);
    this->_snake.push_back(head);
    this->_snake.push_back(body);
    this->_snake.push_back(body2);
    this->_snake.push_back(body3);
    this->_nextPos = RIGHT;

    _directionChooserLeft[LEFT] = DOWN;
    _directionChooserLeft[DOWN] = RIGHT;
    _directionChooserLeft[RIGHT] = UP;
    _directionChooserLeft[UP] = LEFT;

    _directionChooserRight[LEFT] = UP;
    _directionChooserRight[DOWN] = LEFT;
    _directionChooserRight[RIGHT] = DOWN;
    _directionChooserRight[UP] = RIGHT;
    
    _directionChecker[LEFT] = RIGHT;
    _directionChecker[UP] = DOWN;
    _directionChecker[RIGHT] = LEFT;
    _directionChecker[DOWN] = UP;
    
    _score = 0;
}

Snake::Snake(const Snake& orig) {
    this->_snake = orig._snake;
}

Snake::~Snake() {
}

// OPERATORS ===================================================================

Snake& Snake::operator=(const Snake& right) {
    if (this == &right)
        return *this;
    this->_snake = right._snake;
    return *this;
}

// GETTERS & SETTERS ===========================================================

std::vector<Body*> Snake::getSnake() const {
    return this->_snake;
}

const int& Snake::getScore() const {
    return this->_score;
}

void Snake::setNextDirection(const eDirection& nextPos) {
    if (!g_arrows) {
        this->_nextPos = (nextPos == LEFT ? _directionChooserLeft[this->_nextPos] : _directionChooserRight[this->_nextPos]);
        g_flagEvent = 1;
    } else {
        if (this->_directionChecker[nextPos] != this->_nextPos) {
            this->_nextPos = nextPos;
            g_flagEvent = 1;
        }
    }
}

// FUNCTIONS ===================================================================

int Snake::updatePositions() {
    int ret;

    for (int i = _snake.size() - 1; i > 0; --i) {
        if ((ret = _snake.at(i)->setDirectionAndUpdate(_snake.at(i - 1)->getDirection())) == 1)
            return (1);
    }
    if ((ret = _snake.front()->setDirectionAndUpdate(this->_nextPos)) == 1)
        return (1);
    if (ret == FOOD_EATED)
        this->addBody();
    return 0;
}

int* Snake::computeBackPos() {
    int* pos;

    pos = new int[2];
    pos[0] = _snake.back()->getPosX();
    pos[1] = _snake.back()->getPosY();
    switch (_snake.back()->getDirection()) {
        case LEFT:
            pos[0] += 1;
            break;
        case RIGHT:
            pos[0] -= 1;
            break;
        case UP:
            pos[1] += 1;
            break;
        case DOWN:
            pos[1] -= 1;
            break;
        case NONE:
            break;
        default:
            break;
    }
    return pos;
}

void Snake::addBody() {
    eDirection direction;
    int* pos;
    Body* newBody;

    pos = computeBackPos();
    direction = _snake.back()->getDirection();
    newBody = new Body(pos[0], pos[1], REGULAR, direction);
    _snake.push_back(newBody);
    Board::getInstance(1).addFood();
    _score += 10;
    if (_score % 30 == 0)
        Board::getInstance(1).addObstacle();
}

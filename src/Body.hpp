/* 
 * File:   Body.hpp
 * Author: Clement Da Silva
 *
 * Created on March 24, 2015, 11:22 AM
 */

#ifndef BODY_HPP_
# define BODY_HPP_

# include <iostream>

enum eBodyType {
    HEAD,
    REGULAR,
    FOOD,
    OBSTACLE,
    EMPTY
};

enum eDirection {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    NONE
};

class Body {
private:
    int _x;
    int _y;
    eDirection _direction;
    eBodyType _type;
public:
    Body(int x = 0, int y = 0, eBodyType type = EMPTY, eDirection direction = NONE);
    Body(const Body& orig);
    virtual ~Body();

    void setPos(const int& x, const int& y);
    void setDirection(const eDirection& direction);
    int setDirectionAndUpdate(const eDirection& direction);
    
    int getPosX() const;
    int getPosY() const;
    eBodyType getType() const;
    eDirection getDirection() const;
    
    Body& operator=(const Body& right);
};

#endif	/* !BODY_HPP_ */


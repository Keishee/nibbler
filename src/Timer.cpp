/* 
 * File:   timer.cpp
 * Author: barbis_j
 * 
 * Created on March 23, 2015, 5:41 PM
 */

#include "Timer.hpp"

Timer::Timer(const char *dlName, long int dur, unsigned int inter) {
    std::string dl;
    dl = "./";
    dl.append(dlName);
    duration = dur;
    interval = inter;
    start = TimeGiver::timeMiS();
    elapsed = 0;
    try {
        loader = new DLLoader<IDisplayModule>;
        Board::getInstance(1).setDisplayModule(loader->getInstance(dl.c_str()));
    } catch (std::runtime_error& e) {
        throw e;
    }
}

Timer::Timer(const Timer& orig) {
    duration = orig.duration;
    interval = orig.interval;
    elapsed = orig.elapsed;
}

Timer::~Timer() {
}

// OPERATORS ===================================================================

Timer& Timer::operator=(const Timer& orig) {
    if (this == &orig)
        return *this;
    duration = orig.duration;
    interval = orig.interval;
    elapsed = orig.elapsed;
    return *this;
}

// GETTERS & SETTERS ===========================================================

long Timer::GetDuration() const {
    return duration;
}

unsigned long Timer::GetElapsed() const {
    return elapsed;
}

unsigned int Timer::GetInterval() const {
    return interval;
}

void Timer::SetElapsed(unsigned long elapsed) {
    this->elapsed = elapsed;
}

long int Timer::GetStart() const {
    return start;
}

void Timer::SetStart(long int start) {
    this->start = start;
}

void Timer::setInterval(unsigned int inter) {
    if (inter > 10 && inter < TIMER_INTER)
        this->interval = inter;
}


// FUNCTIONS ===================================================================

void Timer::launch() {
    while (this->GetDuration() < 0) {
        this->setInterval(TIMER_INTER - (Board::getInstance(1).getSnake()->getSnake().size() * BONUS_TIME));
        pthread_mutex_unlock(&g_m2);
        if (TimeGiver::timeMiS() - this->GetStart() >= this->GetInterval()) {
            if (this->boardOperations() == DEAD)
                return;
            this->SetStart(TimeGiver::timeMiS());
        }
        if (g_flagEvent == 1) {
            if (this->boardOperations() == DEAD)
                return;
            this->SetStart(TimeGiver::timeMiS());
            g_flagEvent = 0;
        }
        usleep(10);
    }
    while (this->GetElapsed() < static_cast<unsigned long> (this->GetDuration())) {
        this->setInterval(TIMER_INTER - (Board::getInstance(1).getSnake()->getSnake().size() * BONUS_TIME));
        pthread_mutex_unlock(&g_m2);
        if (TimeGiver::timeMiS() - this->GetStart() >= this->GetInterval()) {
            if (boardOperations() == DEAD)
                return;
            this->SetElapsed(this->GetElapsed() + this->GetInterval());
        }
        usleep(10);
    }
}

void Timer::writeScore(int newScore) const {
    std::stringstream stream;
    std::fstream file;
    int score = 0;

    file.open("LeaderBoard.txt", std::fstream::in);
    if (file.is_open()) {
        stream << file;
        file >> score;
        if (newScore > score) {
            file.close();
            file.open("LeaderBoard.txt", std::fstream::out | std::fstream::trunc);
            file << newScore;
        }
        file.close();
    }
}

int Timer::boardOperations() {
    if (Board::getInstance(1).getSnake()->updatePositions() == 1) {
        std::cout << "YOU DIED" << std::endl;
        writeScore(Board::getInstance(1).getSnake()->getScore());
        std::cout << "Score: " << Board::getInstance(1).getSnake()->getScore() << std::endl;
        pthread_mutex_unlock(&g_m2);
        return DEAD;
    }
    Board::getInstance(1).drawBoard();
    pthread_mutex_unlock(&g_m2);
    return ALIVE;
}

/* 
 * File:   Board.cpp
 * Author: barbis_j
 * 
 * Created on March 25, 2015, 1:09 PM
 */

#include "Board.hpp"

pthread_mutex_t g_m1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t g_m2 = PTHREAD_MUTEX_INITIALIZER;
int g_flagEvent = 0;

Board::Board() {
    Body* food1 = new Body(1, 1, FOOD, NONE);
    snake = new Snake();
    food.push_back(food1);
    srand(time(NULL));
}

Board::Board(const Board& orig) {
    (void) orig;
}

Board::~Board() {
}

Board& Board::getInstance(char flag) {
    static Board* boardInstance;

    if (flag == 1) {
        pthread_mutex_lock(&g_m1);
        pthread_mutex_trylock(&g_m2);
        pthread_mutex_unlock(&g_m1);
    } else if (flag == 2) {
        pthread_mutex_lock(&g_m2);
        pthread_mutex_trylock(&g_m1);
        pthread_mutex_unlock(&g_m2);
    }

    if (boardInstance == NULL) {
        boardInstance = new Board;
    }
    return *boardInstance;
}

// OPERATORS ===================================================================

Board& Board::operator=(const Board& orig) {
    if (this == &orig)
        return *this;
    //board = orig.board;
    return *this;
}

// GETTERS & SETTERS ===========================================================

Snake* Board::getSnake() const {
    return this->snake;
}

std::vector<Body*> Board::getFood() const {
    return this->food;
}

std::vector<Body*> Board::getObstacles() const {
    return this->obstacles;
}

void Board::setDisplayModule(IDisplayModule* disp) {
    this->displayModule = disp;
}

IDisplayModule* Board::getDisplayModule() const {
    return displayModule;
}
// FUNCTIONS ===================================================================

void Board::drawBoard() {
    displayModule->updateDisplay(snake, food, obstacles);
}

void Board::addFood() {
    int posx;
    int posy;
    bool end;

    food.clear();
    end = false;
    posx = posy = -1;
    while (!end) {
        posx = rand() % g_sizex;
        posy = rand() % g_sizey;
        end = true;
        for (unsigned int i = 0; i < snake->getSnake().size(); ++i) {
            if (snake->getSnake().at(i)->getPosX() == posx &&
                    snake->getSnake().at(i)->getPosY() == posy) {
                end = false;
                break;
            }
        }
        for (unsigned int i = 0; i < obstacles.size(); ++i) {
            if (obstacles.at(i)->getPosX() == posx &&
                    obstacles.at(i)->getPosY() == posy) {
                end = false;
                break;
            }
        }
    }
    Body *newFood;
    newFood = new Body(posx, posy, FOOD, NONE);
    food.push_back(newFood);
}

void Board::addObstacle() {
    int posx;
    int posy;
    bool end;

    end = false;
    posx = posy = -1;
    while (!end) {
        posx = rand() % g_sizex;
        posy = rand() % g_sizey;
        end = true;
        for (unsigned int i = 0; i < snake->getSnake().size(); ++i) {
            if (snake->getSnake().at(i)->getPosX() == posx &&
                    snake->getSnake().at(i)->getPosY() == posy) {
                end = false;
                break;
            }
        }
        for (unsigned int i = 0; i < food.size(); ++i) {
            if (food.at(i)->getPosX() == posx &&
                    food.at(i)->getPosY() == posy) {
                end = false;
                break;
            }
        }
    }
    Body *newObstacle;
    newObstacle = new Body(posx, posy, OBSTACLE, NONE);
    obstacles.push_back(newObstacle);
}


/* 
 * File:   Errors.hpp
 * Author: Clement Da Silva
 *
 * Created on March 31, 2015, 3:47 PM
 */

#ifndef ERRORS_HPP_
#define ERRORS_HPP_

#include <iostream>

class DLError : public std::exception {
private:
    std::string _what;
public:
    DLError(const std::string& = "DLError") throw ();
    DLError(const DLError& orig) throw ();
    virtual ~DLError() throw ();

    virtual const char* what() const throw ();

    DLError& operator=(const DLError& right);
};

class GraphicError : public std::exception {
private:
    std::string _what;
public:
    GraphicError(const std::string& = "GraphicError") throw ();
    GraphicError(const GraphicError& orig) throw ();
    virtual ~GraphicError() throw ();

    virtual const char* what() const throw ();

    GraphicError& operator=(const GraphicError& right);
};

class StandardError : public std::exception {
private:
    std::string _what;
public:
    StandardError(const std::string& = "StandardError") throw ();
    StandardError(const StandardError& orig) throw ();
    virtual ~StandardError() throw ();

    virtual const char* what() const throw ();

    StandardError& operator=(const StandardError& right);

};
#endif	/* !ERRORS_HPP_ */


/* 
 * File:   Board.hpp
 * Author: barbis_j
 *
 * Created on March 25, 2015, 1:09 PM
 */

#ifndef BOARD_HPP
#define	BOARD_HPP

#include <iostream>
#include <vector>
#include <cstdlib>
#include <pthread.h>
#include "nibbler.hpp"
#include "Snake.hpp"
#include "dlibs/IDisplayModule.hpp"
#include "dlibs/DLLoader.hpp"

extern pthread_mutex_t g_m1;
extern pthread_mutex_t g_m2;
extern int g_flagEvent;

class Timer;

class Board {
private:
    Snake* snake;
    std::vector<Body*> obstacles;
    std::vector<Body*> food;
    IDisplayModule* displayModule;
    Timer *timer;
public:
    Board();
    Board(const Board& orig);
    virtual ~Board();
    Board& operator=(const Board& orig);
    static Board& getInstance(char flag);

    void addFood();
    void addObstacle();
    std::vector<Body*> getFood() const;
    std::vector<Body*> getObstacles() const;
    Snake* getSnake() const;
    IDisplayModule* getDisplayModule() const;
    void setDisplayModule(IDisplayModule*);
    void setTimer(Timer* timer);
    Timer* getTimer() const;

    void drawBoard();

};

#endif	/* BOARD_HPP */


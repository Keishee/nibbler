/* 
 * File:   timer.hpp
 * Author: barbis_j
 *
 * Created on March 23, 2015, 5:41 PM
 */

#ifndef TIMER_HPP
#define	TIMER_HPP

#include <pthread.h>
#include <unistd.h>
#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <fstream>
#include "TimeGiver.hpp"
#include "Board.hpp"
#include "dlibs/DLLoader.hpp"
#include "dlibs/IDisplayModule.hpp"
#include "nibbler.hpp"

class Timer {
public:
    Timer(const char *, long int dur = -1, unsigned int inter = TIMER_INTER);
    Timer(const Timer& orig);
    virtual ~Timer();
    Timer& operator=(const Timer& orig);

    long GetDuration() const;
    unsigned long GetElapsed() const;
    unsigned int GetInterval() const;
    void setInterval(unsigned int);
    long int GetStart() const;
    void SetStart(long int start);
    void SetElapsed(unsigned long elapsed);

    int boardOperations();
    void launch();
private:
    DLLoader<IDisplayModule> *loader;
    unsigned long elapsed;
    long duration;
    unsigned int interval;
    unsigned long long start;
    void writeScore(int) const;
};

void *launchTimer(void *arg);

#endif	/* TIMER_HPP */


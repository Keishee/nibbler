/* 
 * File:   Snake.hpp
 * Author: Clement Da Silva
 *
 * Created on March 25, 2015, 12:20 PM
 */

#ifndef SNAKE_HPP_
#define SNAKE_HPP_

#include <vector>
#include <iostream>
#include <map>
#include "nibbler.hpp"
#include "Body.hpp"

class Snake {
private:
    std::vector<Body *> _snake;
    eDirection _nextPos;
    std::map<eDirection, eDirection> _directionChecker;
    std::map<eDirection, eDirection> _directionChooserLeft;
    std::map<eDirection, eDirection> _directionChooserRight;
    int _score;
    
    int* computeBackPos();
public:
    Snake();
    Snake(const Snake& orig);
    virtual ~Snake();

    int updatePositions();
    void addBody();
    void setNextDirection(const eDirection& nextPos);
    const int& getScore() const;
    
    Snake& operator=(const Snake& right);
    std::vector<Body*> getSnake() const;
};

#endif	/* !SNAKE_HPP_ */


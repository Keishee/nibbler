/* 
 * File:   main.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 23, 2015, 7:14 PM
 */

#include <dlfcn.h>
#include <cstring>
#include "dlibs/IDisplayModule.hpp"
#include "dlibs/DLLoader.hpp"
#include "Timer.hpp"
#include "Board.hpp"
#include "Errors.hpp"
#include "nibbler.hpp"

int g_sizex;
int g_sizey;
int g_entitySizex;
int g_entitySizey;
bool g_arrows;

void argumentChecker(int ac, char **av) {
    g_arrows = false;
    if (ac < 4) {
        throw StandardError("Usage: ./nibbler [sizeX] [sizeY] [lib] {-arrows}");
    }
    if (av[4] && !strcmp(av[4], "-arrows"))
        g_arrows = true;
    if (atoi(av[1]) < 6 || atoi(av[1]) > 30 || atoi(av[2]) < 6 || atoi(av[2]) > 30)
        throw StandardError("Wrong sizeX / sizeY");
}

int main(int ac, char **av) {
    try {
        argumentChecker(ac, av);
        g_sizex = atoi(av[1]);
        g_sizey = atoi(av[2]);
        g_entitySizex = WINDOW_SIZE / g_sizex;
        g_entitySizey = WINDOW_SIZE / g_sizey;
        Timer timer(av[3]);
        timer.launch();
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
        std::cout << "Quitting.." << std::endl;
        return (1);
    }
    return (1);
}
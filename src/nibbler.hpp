/* 
 * File:   nibbler.hpp
 * Author: barbis_j
 *
 * Created on March 23, 2015, 4:29 PM
 */

#ifndef NIBBLER_HPP
# define NIBBLER_HPP

# define BOARD_SIZE                 15
# define WINDOW_SIZE                800
# define ENTITY_SIZE                WINDOW_SIZE / BOARD_SIZE
# define TIMER_INTER                350000
# define BONUS_TIME                 10000
# define FOOD_EATED                 45242
# define DEAD                       666
# define ALIVE                      999
# define FWAY                       1

extern int g_sizex;
extern int g_sizey;
extern int g_entitySizex;
extern int g_entitySizey;
extern bool g_arrows;
extern pthread_mutex_t g_m;

#endif	/* NIBBLER_HPP */

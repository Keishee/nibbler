/* 
 * File:   DLLoader.hpp
 * Author: Clement Da Silva
 *
 * Created on March 23, 2015, 7:21 PM
 */

#ifndef DLLOADER_HPP_
#define DLLOADER_HPP_

#include <iostream>
#include <vector>
#include "IDisplayModule.hpp"

template <typename T>
class DLLoader {
private:
    void* handler;
public:
    ~DLLoader();
    T* getInstance(std::string const& lib, const std::string& sym = "createDisplayModule");
};

#endif	/* !DLLOADER_HPP_ */


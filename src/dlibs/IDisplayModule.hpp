/* 
 * File:   IDisplayModule.hpp
 * Author: Clement Da Silva
 *
 * Created on March 23, 2015, 7:01 PM
 */

#ifndef IDISPLAYMODULE_HPP_
# define IDISPLAYMODULE_HPP_

#include <iostream>
#include <vector>
#include "../Snake.hpp"
#include "../Body.hpp"

class IDisplayModule {
public:
    virtual ~IDisplayModule();
    virtual void updateDisplay(const Snake*, const std::vector<Body*>&,
                               const std::vector<Body*>&) = 0;
    virtual void playSoundObstacle() = 0;
};

#endif	/* IDISPLAYMODULE_HPP_ */


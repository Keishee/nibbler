/* 
 * File:   DLLoader.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 23, 2015, 7:21 PM
 */

#include <dlfcn.h>
#include <cstdio>
#include <stdexcept>
#include "../Errors.hpp"
#include "DLLoader.hpp"
#include "IDisplayModule.hpp"

template<typename T>
DLLoader<T>::~DLLoader() {
}

template <typename T>
T* DLLoader<T>::getInstance(const std::string& lib, const std::string& name) {
    T * (*ptr)();

    handler = dlopen(lib.c_str(), RTLD_LAZY);
    if (handler == NULL) {
        throw DLError("Error opening dynamic library");
    }
    ptr = reinterpret_cast<T * (*)()> (dlsym(handler, name.c_str()));
    if (ptr == NULL) {
        throw DLError("Error loading symbol");
    }
    return ptr();
}

template DLLoader<IDisplayModule>::~DLLoader();
template IDisplayModule* DLLoader<IDisplayModule>::getInstance(const std::string& lib, const std::string& sym = "createDisplayModule");